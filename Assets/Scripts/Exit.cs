﻿using UnityEngine;

[RequireComponent(typeof(Cube))]
public class Exit : MonoBehaviour
{
    internal Cube cube;

    private Game game;

    void Awake()
    {
        cube = GetComponent<Cube>();
    }

    Player player;
    Color exit_color = Color.white;
    // Start is called before the first frame update
    void Start()
    {
        game = FindObjectOfType<Game>();
    }

    bool PlayerIsReadyForExit()
    {
        return player.cube.color.Equals(exit_color);
    }

    // Update is called once per frame
    void Update()
    {
        float radius_of_visibility = 8;
        float distance_to_player = (player.transform.position - transform.position).magnitude;
        float delta_alpha = Mathf.Max(0, distance_to_player - radius_of_visibility/2);
        delta_alpha = Mathf.Min(radius_of_visibility, delta_alpha)/radius_of_visibility;

        Color temp_color = cube.color;
        temp_color.a = PlayerIsReadyForExit() ? 1 : delta_alpha;
        cube.color = temp_color;
    }

    public void Init(Color color, Player player)
    {
        exit_color = color;
        cube.color = color;
        this.player = player;
    }
    
    void OnTriggerEnter(Collider other)
    {
        var player = other.gameObject.GetComponent<Player>();
        if(player != null && PlayerIsReadyForExit())
        {
            game.fadePanel.SetActive(true);
            game.statusText.text = "YOU WIN";
            game.statusText.color = new Color(0, 255, 0);
            Game.GameOver(true);
        }           
    }
}
