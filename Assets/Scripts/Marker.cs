﻿using UnityEngine;

[RequireComponent(typeof(Cube))]
public class Marker : MonoBehaviour
{
    internal Cube cube;
    void Awake()
    {
        cube = GetComponent<Cube>();
    }

    private Vector3 speed;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        cube.Move(speed.normalized);
    }
    
    public void Init(Color color, Vector3 position, Vector3 speed)
    {
        cube.color = color;
        transform.position = position;
        this.speed = speed;
    }
    
    void OnTriggerEnter(Collider other)
    {
        var player = other.gameObject.GetComponent<Player>();
        if (player != null)
        {
            player.SwallowColor(cube.color);
            Destroy(gameObject);
        }
    }
}
